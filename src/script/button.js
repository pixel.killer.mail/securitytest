const buttonTemplate = document.createElement('template')

buttonTemplate.innerHTML = 
  `
    <button></button>
  `

class componentButton extends HTMLElement{
  constructor (){
    super()
    this.root = this.attachShadow({mode: 'open'})
    this.root.appendChild(buttonTemplate.content.cloneNode(true))
    this.button = this.root.querySelector('button')

    this.labelValue = ''

    this.button.addEventListener('click', (event)=>{
      this.sendEvent(event)
    })
  }

  static get observedAttributes(){
    return ['label']
  }

  attributeChangedCallback(name, _oldVal, newVal){
    this.root.querySelector('button').innerHTML = newVal 
    this.labelValue = newVal
  }

  sendEvent(event){
    this.dispatchEvent(
      new CustomEvent(this.labelValue, {
        bubbles: true
      })
    )
  }
}

window.customElements.define('component-button', componentButton)