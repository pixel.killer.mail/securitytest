const express = require ( 'express' )
const path = require ( 'path' )

const app = express ()

app.set ( 'port', process.env.PORT || 3000 )
app.set ('views', path.join(__dirname, 'views'))
app.set ( 'components', path.join ( __dirname, 'components'))

app.use ( express.urlencoded ( {extended: true } ) )

app.set ( express.static ( path.join( __dirname, 'public' ) ) )
app.use('/script', express.static(path.join(__dirname, 'script')));
app.use ( require ( './routes/index' ) )

module.exports = app